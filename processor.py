import BOFastAGI.common as common
import logging
import ConfigParser
import json
import traceback
from utils import writeaccess
import threading

class BOFastAGIProcessor(object):
	ast_version = ['1.2','1.4','1.6','1.8','11','12','13']
	class_to_execute = None
	verbosity = 3

	def __init__(self):
		self.logger = logging.getLogger("BOFastAGIModule [%s]" % self.__class__.__name__)
		self.config = {}
		self.timeout = common.config.get('timeout')
		
		if common.moduleConfigs.has_key(self.__class__.__name__):
			self.logger.info('Loading config file [%s]' % common.moduleConfigs[self.__class__.__name__])
			self._loadConfig(common.moduleConfigs[self.__class__.__name__])

			if self.config.has_key('log'):
				if self.config['log'].has_key('file'):
					if self.config['log'].has_key('level'):
						self.logger.setLevel(self.config['log']['level'])
					else:
						self.logger.setLevel(logging.INFO)

					fh = logging.FileHandler(self.config['log']['file'])
					formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s: %(message)s',datefmt='%d/%m/%Y %H:%M:%S')
					fh.setFormatter(formatter)
					self.logger.addHandler(fh)
					self.logger.propagate=False

			if self.config.get('general'):
				if self.config['general'].get('timeout'):
					try:
						self.timeout = int(self.config['general'].get('timeout'))
					except ValueError:
						self.timeout = None
		else:
			self.logger.info('Skip loading config file [NOT CONFIGURED]')

	def _loadConfig(self,fname):
		cp = ConfigParser.ConfigParser()

		try:
			f = open(common.moduleConfigs[self.__class__.__name__],'r')
		except IOError:
			f = None
			self.logger.info('Cannot read config file')

		if f:
			try:
				cp.readfp(f)

				for s in cp.sections():
					self.config[s]={}
					for o in cp.options(s):
						self.config[s][o]=cp.get(s,o)

				f.close()

				self.logger.info('Config file loaded correctly')
				self.logger.debug(json.dumps(self.config))
			except:
				self.logger.debug(traceback.format_exc())
				self.logger.info('Error loading config')

	def __del__(self):
		pass

	def process(self,agi):
		execute = 0

		for supported_version in self.ast_version:
			if agi.env['agi_version'].startswith(supported_version):
				execute = 1

		agi.set_variable('BOFASTAGI_STATUS', 'ERROR')
		agi.env['bofastagi_retcode']=0
		agi.env['bofastagi_hangup']=0
		agi.env['bofastagi_error']=0

		if execute==1:
			if agi.env['agi_network_script'] in self.app_provided:
				try:
					self.logger.debug('Timeout %s' % self.timeout)
					t = threading.Thread(target=self.run,args=[agi])
					t.setDaemon(True)
					t.start()
					t.join(self.timeout)
					
					if t.isAlive():
						writeaccess(agi,'TIMEOUT')
						agi.set_variable('BOFASTAGI_STATUS', 'TIMEOUT')
						agi.set_variable('BOFASTAGI_RETCODE', agi.env['bofastagi_retcode'])
						return('-1')
					else:
						if agi.env['bofastagi_error']==0 or agi.env['bofastagi_error']==None:
							writeaccess(agi,'SUCCESS')
							agi.set_variable('BOFASTAGI_STATUS', 'SUCCESS')
						else:
							writeaccess(agi,'ERROR')
							agi.set_variable('BOFASTAGI_STATUS', 'ERROR')

						agi.set_variable('BOFASTAGI_RETCODE', agi.env['bofastagi_retcode'])

						if agi.env['bofastagi_hangup']==1:
							agi.hangup()
							return
				except:					
					writeaccess(agi,'INTERROR')
					agi.set_variable('BOFASTAGI_STATUS', 'INTERROR')
					agi.set_variable('BOFASTAGI_RETCODE', agi.env['bofastagi_retcode'])
			else:
				writeaccess(agi,'NOTFOUND')
				agi.verbose('BOFastAGIServer: App [%s] not found' % agi.env['agi_network_script'],3)
				agi.set_variable('BOFASTAGI_STATUS', 'NOTFOUND')
				agi.set_variable('BOFASTAGI_RETCODE', agi.env['bofastagi_retcode'])
		else:
			writeaccess(agi,'ASTVERNOTSUPPORTED')
			agi.verbose('BOFastAGIServer: Asterisk version [%s] not supported' % agi.env['agi_version'],3)
			agi.set_variable('BOFASTAGI_STATUS', 'ASTVERNOTSUPPORTED')
			agi.set_variable('BOFASTAGI_RETCODE', agi.env['bofastagi_retcode'])

	def run(self, agi):
		if self.class_to_execute:
			try:
				inner = self.class_to_execute(agi)

				if inner.init_ok==True:
					res = inner.run()

					if res==None:
						res = 0

					if res!=0:
						agi.env['bofastagi_hangup']=1
						agi.env['bofastagi_error']=res
						agi.verbose('[%s] Inner return: [%s]' % (self.__class__.__name__, res), 1)
						return
				else:
					agi.verbose('[%s] Init Failed' % self.__class__.__name__, 1)
					agi.env['bofastagi_hangup']=1
					return
			except:
				agi.verbose("[%s] == Exception" % (self.__class__.__name__), self.verbosity)
				for l in traceback.format_exc().replace('"',"'").split('\n'):
					agi.verbose( "[%s]    + %s" % (self.__class__.__name__, l), self.verbosity)
		else:
			agi.verbose("[%s] == Class to execute handler not setted, Hangup!!" % (self.__class__.__name__), self.verbosity)
			agi.env['bofastagi_hangup']=1
			agi.env['bofastagi_error']=-1
			return
