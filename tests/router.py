from BOFastAGIServer.router import BOFastAGIRouter
import logging
import sys

logging.basicConfig(stream=sys.stdout, level=logging.DEBUG)

router = BOFastAGIRouter()
route = router.getRoute(sys.argv[1])

if route:
	print route.run()
else:
	print "Route:[%s] not found" % sys.argv[1]

del(router)