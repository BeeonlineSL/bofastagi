import SocketServer
from pyagi.pyagi import AGI, re_code, re_kv
import threading
import time
import logging
import BOFastAGI.common as common
from pyagi.exceptions import *
from utils import writeaccess

class BOFastAGIServerParser(AGI):
    def __init__(self,rfile,wfile):
        self.rfile = rfile
        self.wfile = wfile
        self.env = {}
        self._get_agi_env()

    def _get_agi_env(self):
        while True:

            line = self.rfile.readline().strip()
            if not line:
                break

            key, data = line.split(':')[0], ':'.join(line.split(':')[1:])
            key, data = (x.strip() for x in (key, data))

            if key:
                self.env[key] = data

    def _quote(self, string):
        return ''.join(['"', str(string).replace('"',"'"), '"'])    

    def send_command(self, command, *args):
        command = command.strip()
        command = '%s %s' % (command, ' '.join(map(str,args)))
        command = command.strip()
        if command[-1] != '\n':
            command += '\n'
        self.wfile.write(command)
        self.wfile.flush()

    def get_result(self, stdin=None):
        code = 0
        result = {'result':('','')}
        line = self.rfile.readline().strip()
        m = re_code.search(line)
        if m:
            code, response = m.groups()
            try:
                code = int(code)
            except ValueError:
                raise AGIAppError("Error executing application by hangup")

        if code == 200:
            for key,value,data in re_kv.findall(response):
                result[key] = (value, data)

                if data == 'hangup':
                    raise AGIResultHangup("User hangup during execution")

                if key == 'result' and value == '-1':
                    raise AGIAppError("Error executing application, or hangup")

            return result
        elif code == 510:
            raise AGIInvalidCommand(response)
        elif code == 520:
            usage = [line]

            usage.append(line)
            line = self.rfile.readline().strip()
            usage.append(line)
            usage = '%s\n' % '\n'.join(usage)
            raise AGIUsageError(usage)
        else:
            raise AGIUnknownError(code, 'Unhandled code or undefined response')
	
class BOFastAGIServerHandler(SocketServer.StreamRequestHandler):
    def handle(self):
        self.timeout = common.config['timeout']
        agi = BOFastAGIServerParser(self.rfile, self.wfile)

        logging.debug("AGI session established. Client address [%s]" % self.client_address[0])
        logging.debug("AGI enviroment: [%s]" % (agi.env))

        if (agi.env.has_key('agi_network_script')):
            agi.env['bofastagi_client_address']=self.client_address
            route = common.router.getRoute(agi.env['agi_network_script'])    

            if route:
                route.process(agi)
            else:
                writeaccess(agi,'NOTFOUND')
                agi.verbose('BOFastAGIServer: App [%s] not found' % agi.env['agi_network_script'],3)

        else:
            route = common.router.getRoute('default')

            if route:
                route.process(agi)
            else:
                agi.verbose('[BOFastAGIServer: Default App not found',3)

class ThreadedTCPServer(SocketServer.ThreadingMixIn, SocketServer.TCPServer):
    daemon_threads = True
    allow_reuse_address = True

class BOFastAGIServer(object):
    def __init__(self,ip,port):
        try:
            self.server = ThreadedTCPServer((ip,port),BOFastAGIServerHandler)
            self.server_thread = threading.Thread(target=self.server.serve_forever)
            self.server_thread.daemon = True
        except KeyboardInterrupt, e:
            common.bofas.stop()
            raise SystemExit
            
    def start(self):
        self.server_thread.start()

    def stop(self):
        self.server.shutdown()
