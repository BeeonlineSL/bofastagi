from BOFastAGI.processor import BOFastAGIProcessor
from pyagi.exceptions import *
import BOFastAGI.common as common
import traceback

class default(BOFastAGIProcessor):
	app_provided = ['default']

	def run(self,agi):
		try:
			agi.verbose("Executing default App",1)
			self.logger.info(agi.env)
		except:
			agi.verbose("[%s] == Exception" % self.__class__.__name__,1)
			for l in traceback.format_exc().split('\n'):
				agi.verbose( "[%s]    + %s" % (self.__class__.__name__,l),1)
