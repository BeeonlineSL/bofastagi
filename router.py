import BOFastAGI.common as common
import sys
import logging
import ConfigParser
import traceback

class BOFastAGIRouter(object):
	def __init__(self):
		self.logger = logging.getLogger('BOFastAGIRouter')
		self.routes = {}
		self.routing = {}
		self.modules = {}

		self._loadConfig()
		self._initAllRoutes()

	def _loadConfig(self):
		if common.config_path:
			fname = "%s/routes.config" % common.config_path
		else:
			fname = "/etc/BOFastAGIServer/routes.config"

		try:
			f = open(fname,'r')
		except:
			self.logger.error("Config file '%s' not found" % fname)
			sys.exit(-1)

		cp = ConfigParser.ConfigParser()
		cp.readfp(f)

		try:
			common.modules_path = cp.get('general', 'module_path')
		except ConfigParser.NoOptionError:
			common.modules_path = "/usr/local/lib/BOFastAGI/"
			self.logger.info("Config file '%s' in section [general] option 'module_path' not found using '/usr/local/lib/BOFastAGIServer/' as default" % fname)
		
		if common.modules_path not in sys.path:
			sys.path.append(common.modules_path)

		for module in cp.sections():
			if module!='general':		
				try:
					tmpname = module
					tmpfname = cp.get(module,'module_file')
					tmproute = cp.get(module,'route')

					try:
						cfname = cp.get(module,'config_file').strip()

						if cfname[0] != '/':
							cfname = '%s/%s' % (common.config_path,cfname)

						common.moduleConfigs[tmpname] = cfname
					except ConfigParser.NoOptionError:
						pass

					try:
						if tmpfname in sys.modules:
							reload(self.modules[tmpfname]['module'])
							self.routes[tmpname] = {"route": tmproute, "name": tmpname, "module_file": tmpfname, "baseClass": getattr(self.modules[tmpfname]['module'],tmpname)}
						else:
							mm = __import__(tmpfname,fromlist=(tmpname))
							
							if self.modules.has_key(tmpfname):
								self.modules[tmpfname]['routes'].append(tmpname)
							else:
								self.modules[tmpfname]={'routes':[tmpfname], 'module': mm}

							self.routes[tmpname] = {"route": tmproute, "name": tmpname, "module_file": tmpfname, "baseClass": getattr(mm,tmpname)}
						
						self.logger.info("Module:[%s] loaded" % module)
					except:
						self.logger.info("Module:[%s] not loaded. Reason: %s" % (module, traceback.format_exc()))

				except ConfigParser.NoOptionError:
					self.logger.info("Module:[%s] not loaded. Reason: Not all needed arguments provided" % module)

	def _initAllRoutes(self):
		for route in self.routes:
			self.__initRoute(route)

	def _delAllRoutes(self):
		for route in self.routes:
			self.__delRoute(route)

	def __initRoute(self,route):
		self.__delRoute(route)
		self.routing[self.routes[route]['route']]=self.routes[route]['baseClass']()

	def __delRoute(self,route):
		if self.routing.has_key(route):
			del(self.routing[route])

	def reloadConfig(self):
		for route in self.routes:
			self.__delRoute(route)
		
		del(self.routes)
		self.routes = {}

		self._loadConfig()
		self._initAllRoutes()

	def getRoute(self,route):
		if self.routing.has_key(route):
			return self.routing[route]
		else:
			return None
