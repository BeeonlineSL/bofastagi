try:
	import MySQLdb
except:
	pass

try:
	import pyodbc
except:
	pass

import BOFastAGI.common as common
import sys, traceback
import ConfigParser
import logging

class BODBFactory(object):
	def __init__(self):
		self.logger = logging.getLogger('BODBFactory')
		self.dbs = {}

		self._loadConfig()
		self._connectAllDB()

	def _loadConfig(self):
		if common.config_path:
			fname = "%s/db.config" % common.config_path
		else:
			fname = "/etc/BOFastAGIServer/db.config"

		try:
			f = open(fname,'r')
		except:
			self.logger.error("Config file '%s' not found" % fname)
			sys.exit(-1)
		
		cp = ConfigParser.ConfigParser()
		cp.readfp(f)

		for dbname in cp.sections():
			if cp.get(dbname,'type')=='mysql':
				try:
					tmphost = cp.get(dbname, 'host')

					try:
						tmpport = cp.get(dbname, 'port')
					except ConfigParser.NoOptionError:
						tmpport = 3306

					tmpuser = cp.get(dbname, 'user')

					try:
						tmppassword = cp.get(dbname, 'password')
					except ConfigParser.NoOptionError:
						tmppassword = None

					try:
						tmpdb = cp.get(dbname, 'db')
					except ConfigParser.NoOptionError:
						tmpdb = None

					if tmphost and tmpport and tmpuser:
						self.dbs[dbname] = {'type': 'mysql', 'host': tmphost, 'port': tmpport, 'user': tmpuser, 'password': tmppassword, 'db': tmpdb, 'connector': None}
					else:
						self.logger.info("DB Name:[%s] Type:[%s] ignored. Reason: not all necesaries args present" % (dbname, cp.get(dbname, 'type')))
				except ConfigParser.NoOptionError:
					self.logger.info("DB Name:[%s] Type:[%s] ignored. Reason: not all necesaries args present" % (dbname, cp.get(dbname, 'type')))

			elif cp.get(dbname,'type')=='odbc':
				try:
					tmpdsn = cp.get(dbname,'dsn')
					tmpuser = cp.get(dbname,'user')
					tmppassword = cp.get(dbname,'password')
					tmpdb = cp.get(dbname,'db')

					if tmpdsn and tmpuser and tmppassword and tmpdb:
						self.dbs[dbname] = {'type': 'odbc', 'dsn': tmpdsn, 'user': tmpuser, 'password': tmppassword, 'db': tmpdb, 'connector': None}
					else:
						self.logger.info("DB Name:[%s] Type:[%s] ignored. Reason: not all necesaries args present" % (dbname, cp.get(dbname, 'type')))
				except ConfigParser.NoOptionError:
					self.logger.info("DB Name:[%s] Type:[%s] ignored. Reason: not all necesaries args present" % (dbname, cp.get(dbname, 'type')))

			else:
				self.logger.info("DB Name:[%s] Type:[%s] ignored. Reason: type unknown" % (dbname, cp.get(dbname, 'type')))

	def __connectDB(self,dbname):
		if self.dbs.has_key(dbname):
			if self.dbs[dbname]['connector']:
				self.__disconnectDB(dbname)

			if self.dbs[dbname]['type']=='mysql':
				if MySQLdb:
					try:
						if self.dbs[dbname]['db']:
							self.dbs[dbname]['connector']=MySQLdb.connect(host=self.dbs[dbname]['host'],user=self.dbs[dbname]['user'],passwd=self.dbs[dbname]['password'],port=self.dbs[dbname]['port'],db=self.dbs[dbname]['db'])
						else:
							self.dbs[dbname]['connector']=MySQLdb.connect(host=self.dbs[dbname]['host'],user=self.dbs[dbname]['user'],passwd=self.dbs[dbname]['password'],port=self.dbs[dbname]['port'])
					except MySQLdb.OperationalError, e:
						self.logger.error('Error connecting DB [%s], traceback: %s' % (dbname, e[1]))
				else:
					self.logger.error('Module MySQLdb not load, cannot connect to DB [%s]' % dbname)
			elif self.dbs[dbname]['type']=='odbc':
				if pyodbc:
					connect_string = "DSN=%s;UID=%s;PWD=%s" % (self.dbs[dbname]['dsn'],self.dbs[dbname]['user'],self.dbs[dbname]['password'])
					if self.dbs[dbname]['db']:
						connect_string += ';DATABASE=%s' % self.dbs[dbname]['db']

					try:
						self.dbs[dbname]['connector']=pyodbc.connect(connect_string)
					except pyodbc.ProgrammingError, e:
						self.logger.error('Error connecting DB [%s], traceback: %s' % (dbname, e[1]))	
				else:
					self.logger.error('Module pyodbc not load, cannot connect to DB [%s]' % dbname)

	def __checkDB(self,dbname):
		if self.dbs.has_key(dbname):
			if self.dbs[dbname]['connector']:
				db = self.dbs[dbname]['connector']
				c = db.cursor()
				try:
					c.execute("SELECT 1")

					if c.fetchone():
						return c
					else:
						return False
				except:
					self.__disconnectDB(dbname)
			else:
				return False
		else:
			return False

	def __disconnectDB(self,dbname):
		if self.dbs.has_key(dbname):
			if self.dbs[dbname]['connector']:
				try:
					self.dbs[dbname]['connector'].close()
				except:
					self.dbs[dbname]['connector']=None

	def getConnector(self,dbname):
		if self.dbs[dbname]['type']=='mysql':
			if MySQLdb:
				try:
					if self.dbs[dbname]['db']:
						return MySQLdb.connect(host=self.dbs[dbname]['host'],user=self.dbs[dbname]['user'],passwd=self.dbs[dbname]['password'],port=self.dbs[dbname]['port'],db=self.dbs[dbname]['db'])
					else:
						return MySQLdb.connect(host=self.dbs[dbname]['host'],user=self.dbs[dbname]['user'],passwd=self.dbs[dbname]['password'],port=self.dbs[dbname]['port'])
				except MySQLdb.OperationalError, e:
					self.logger.error('Error connecting DB [%s], traceback: %s' % (dbname, e[1]))
					return None
			else:
				self.logger.error('Module MySQLdb not load, cannot connect to DB [%s]' % dbname)
				return None
		elif self.dbs[dbname]['type']=='odbc':
			if pyodbc:
				connect_string = "DSN=%s;UID=%s;PWD=%s" % (self.dbs[dbname]['dsn'],self.dbs[dbname]['user'],self.dbs[dbname]['password'])
				if self.dbs[dbname]['db']:
					connect_string += ';DATABASE=%s' % self.dbs[dbname]['db']

				try:
					return pyodbc.connect(connect_string)
				except pyodbc.ProgrammingError, e:
					self.logger.error('Error connecting DB [%s], traceback: %s' % (dbname, e[1]))	
					return None
			else:
				self.logger.error('Module pyodbc not load, cannot connect to DB [%s]' % dbname)
				return None
		else:
			self.logger.error('Connector of type %s not supported' % self.dbs[dbname]['type'])
			return None

	def getConnector(self,dbname):
		if self.dbs[dbname]['type']=='mysql':
			if MySQLdb:
				try:
					if self.dbs[dbname]['db']:
						return MySQLdb.connect(host=self.dbs[dbname]['host'],user=self.dbs[dbname]['user'],passwd=self.dbs[dbname]['password'],port=self.dbs[dbname]['port'],db=self.dbs[dbname]['db'])
					else:
						return MySQLdb.connect(host=self.dbs[dbname]['host'],user=self.dbs[dbname]['user'],passwd=self.dbs[dbname]['password'],port=self.dbs[dbname]['port'])
				except MySQLdb.OperationalError, e:
					self.logger.error('Error connecting DB [%s], traceback: %s' % (dbname, e[1]))
					return None
			else:
				self.logger.error('Module MySQLdb not load, cannot connect to DB [%s]' % dbname)
				return None
		elif self.dbs[dbname]['type']=='odbc':
			if pyodbc:
				connect_string = "DSN=%s;UID=%s;PWD=%s" % (self.dbs[dbname]['dsn'],self.dbs[dbname]['user'],self.dbs[dbname]['password'])
				if self.dbs[dbname]['db']:
					connect_string += ';DATABASE=%s' % self.dbs[dbname]['db']

				try:
					return pyodbc.connect(connect_string)
				except pyodbc.ProgrammingError, e:
					self.logger.error('Error connecting DB [%s], traceback: %s' % (dbname, e[1]))	
					return None
			else:
				self.logger.error('Module pyodbc not load, cannot connect to DB [%s]' % dbname)
				return None
		else:
			self.logger.error('Connector of type %s not supported' % self.dbs[dbname]['type'])
			return None



	def getCursor(self,dbname):
		c = self.__checkDB(dbname)

		if c:
			return c
		else:
			self.__connectDB(dbname)
			c = self.__checkDB(dbname)

			if c:
				return c

			self.logger.error('Cannot connect to DB [%s]' % dbname)
			return None

	def _connectAllDB(self):
		for db in self.dbs:
			self.__connectDB(db)

	def _disconnectAllDB(self):
		for db in self.dbs:
			self.__disconnectDB(db)

	def __del__(self):
		self._disconnectAllDB()
