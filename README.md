README
------

BOFastAGIServer 0.2

This is the first release of a FastAGI Server fro Asterisk enviromets where the proxies
execute a lot of Asterisk AGI scripts to control the call flow and/or take decision in the
dialplan based on AGI results.

In case that there are a lot of AGI calls in the proxies the system load start to go high.

With this scenario in mind we create a fast, easy to extend and reliable server and here
it's the result.

We based on a AGI implementation by Randall Degges (pyagi) that we used for years with 
virtually 0 problems and adapt it for the use of the native Python TCP Server for the
creation of the server.

Have fun coding.

// DarkS


LICENSE
-------

This project is published under the GPLv3 license attach in LICENSE.md


DEPENDENCIES
------------

    * py_Asterisk (https://pypi.python.org/pypi/py-Asterisk) (only if manager.py is loaded)
    * MySQLdb (https://pypi.python.org/pypi/MySQL-python)
    * pyodbc (https://pypi.python.org/pypi/pyodbc)
    * pyagi (https://github.com/rdegges/pyagi/tree/develop)


INSTALL
-------

    * mkdir -p /usr/local/lib/python2.7/dist-packages
    * git clone https://bitbucket.org/DarkS/beeonline-fastagi-server.git BOFastAGI 
    * mkdir -p /var/run/BOFastAGI/
    * ln -s /usr/local/lib/python2.7/dist-packages/BOFastAGI/conf /etc/BOFastAGI
    * ln -s /usr/local/lib/python2.7/dist-packages/BOFastAGI/main.py /usr/local/bin/BOFastAGI
    * ln -s /usr/local/lib/python2.7/dist-packages/BOFastAGI/modules /usr/local/lib/BOFastAGI


USAGE
-----

BOFastAGI (start|stop|restart)



WRITING AND CONFIGURE MODULES
-----------------------------

To write modules, you have to inherit from the class BOFastAGIProcessor and override the 
run method, ast_version and app_provided. 

Example:

```
#!python

from BOFastAGI.processor import BOFastAGIProcessor

class mymodule(BOFastAGIProcessor):
    app_provided = ['mymodule']
    ast_version = ['1.8','11','13']

    def run(self,agi):
        try:
            if agi.env.has_key('agi_arg_1'):
                agi.verbose("Parameter 1 [%s]" % self.env['agi_arg_1'], 3)
                agi.set_variable("server_error","0")
            else:
                agi.verbose("Parameter 1 required and not present",3)
                agi.set_variable("server_error","2")
        except:
            agi.verbose("App Exception: %s" % traceback.format_exc(), 3)
            agi.set_variable("server_error","1")

```

To activate this module you have to edit /etc/BOFastAGI/routes.conf and add the next section:


```
#!config

;Name of the class to import
[mymodule] 
;Name of the py file without extension located in module directory
module_file = mymodule 
;Route that provided by the module, it have to be contained in app_provided in the module class
route = contacts 
;Config file, it contain the config file name in relative to main config path or absolute path 
config_file = 

```


DATABASE CONFIGURATION
----------------------

In dbs.config we define the database connections that the BODBFactory class keep connected,
actually we support MySQL and ODBC connections, provided by MySQLdb and pyodbc modules.

The format in the config file is very simple.


```
#!config

[mysql_connector]
type = mysql
host = <ip or hostname>
port = 3306 ;optional
user = <username>
password = <password>
db = <database name> ;optional

[odbc_connector]
type = odbc
dsn = <odbc.ini servername>
user = <username>
password = <password>
db = <database name> ;optional
```



DATABASE USAGE
--------------

In the server start, the database config is loaded from the config file and make the first connection
to every database configure. With this the instance of the BODBFactory is store in BOFastAGI.common.dbs.

The way to operate with configure databases is:

    * BOFastAGI.common.dbs.getCursor(dbname), this function return a cursor of the given dbname 
      or None if the connection to the database it's not possible. This method is recommended for
      pulling data from the database and it's faster that the alternative method because the
      connection to the database it's already established.

The connector of MySQLdb are not thread safe and the only way that share a DB connection is to make a new
cursor for every thread that need the access to the DB, because the cursors are thread safe and the connector
not.



CONTRIBUTORS
------------

Randall Degges (rdegges.com) () for the great pyagi module that we use for years

Sander Marechal (jejik.com) (s.marechal@jejik.com) for create the Daemon class that we use to start the server

My sons (Javier and Marta), they're the best, i love them