class BOAGIException(Exception): pass
class BOAGIError(BOAGIException): pass
class BOAGIUnknownError(BOAGIError): pass
class BOAGIAppError(BOAGIError): pass
class BOAGIHangup(BOAGIAppError): pass
class BOAGIResultHangup(BOAGIHangup): pass
class BOAGIDBError(BOAGIAppError): pass
class BOAGIUsageError(BOAGIError): pass
class BOAGIInvalidCommand(BOAGIError): pass

#need to add code error 500 to class
class BOAGI(object):
	DEFAULT_RECORD = 20000
	DEFAULT_TIMEOUT = 2000

	def __init__(self, protocol, transport):
		self.protocol = protocol
		self.transport = transport
		self.env = self.protocol.env
		self.argv = self.protocol.argv

	def close(self):
		self.transport.close()

	def __del__(self):
		self.close()
		del(self.protocol)
		del(self.transport)
		del(self.env)
		del(self.argv)

	def _send_execute(self, command):
		yield self.protocol.send_command(command)

	def _execute(self, command, *args):
		tmp = [command.strip()]
		for arg in args:
			tmp.append(str(arg).strip())

		res = self._send_execute(tmp)
		
		return next(res)

	def _quote(self, string):
		return ''.join(['"', str(string), '"'])

	def _process_digit_list(self, digits):
		if type(digits) == ListType:
			digits = ''.join(map(str, digits))
		return self._quote(digits)

	def answer(self):
		res = self._execute('ANSWER')
		return res.result

	def channel_status(self, channel=''):
		try:
		   res = self.execute('CHANNEL STATUS', channel).result
		except BOAGIAppError:
		   res = 1

		return res

	def control_stream_file(self, filename, escape_digits='', skipms=3000, fwd='', rew='', pause=''):
		escape_digits = self._process_digit_list(escape_digits)
		response = self._execute('CONTROL STREAM FILE', self._quote(filename), escape_digits, self._quote(skipms), self._quote(fwd), self._quote(rew), self._quote(pause))
		res = response.result
		
		if res == 0:
			return ''
		else:
			try:
				return chr(int(res))
			except:
				raise BOAGIError('Unable to convert result to char: %s' % res)

	def database_del(self, family, key):
		result = self.execute('DATABASE DEL', self._quote(family), self._quote(key))
		res = result.result
		value = result.data
		if res == 0:
			raise BOAGIDBError('Unable to delete from database: family=%s, key=%s' % (family, key))

	def database_deltree(self, family, key=''):
		result = self._execute('DATABASE DELTREE', self._quote(family), self._quote(key))
		res = result.result
		value = result.data
		
		if res == 0:
			raise BOAGIDBError('Unable to delete tree from database: family=%s, key=%s' % (family, key))

	def database_get(self, family, key):
		family = '"%s"' % family
		key = '"%s"' % key
		result = self._execute('DATABASE GET', self._quote(family), self._quote(key))
		res = result.result
		value = result.data

		if res == 0:
			raise BOAGIDBError('Key not found in database: family=%s, key=%s' % (family, key))
		elif res == 1:
			return value
		else:
			raise BOAGIError('Unknown exception for : family=%s, key=%s, result=%s' % (family, key, pprint.pformat(result)))

	def database_put(self, family, key, value):
		result = self._execute('DATABASE PUT', self._quote(family), self._quote(key), self._quote(value))
		res = result.result
		value = result.data

		if res == 0:
			raise BOAGIDBError('Unable to put vaule in databale: family=%s, key=%s, value=%s' % (family, key, value))

	def appexec(self, application, options=''):
		result = self._execute('EXEC', application, self._quote(options))
		print result
		res = result.result
		if res == -2:
			raise BOAGIAppError('Unable to find application: %s' % application)
		return res

	def get_data(self, filename, timeout=DEFAULT_TIMEOUT, max_digits=255):
		return self._execute('GET DATA', filename, timeout, max_digits).result

	def get_full_variable(self, name, channel = None):
		try:
			if channel:
				result = self._execute('GET FULL VARIABLE', self._quote(name), self._quote(channel))
			else:
				result = self._execute('GET FULL VARIABLE', self._quote(name))
			
			res = result.result
			value = result.data
		except BOAGIResultHangup:
			res = 1
			value = 'hangup'

		return value

	def get_option(self, filename, escape_digits='', timeout=0):
		escape_digits = self._process_digit_list(escape_digits)
		if timeout:
			response = self._execute('GET OPTION', filename, escape_digits, timeout)
		else:
			response = self._execute('GET OPTION', filename, escape_digits)

		res = response-result
		
		if res == 0:
			return ''
		else:
			try:
				return chr(int(res))
			except:
				raise BOAGIError('Unable to convert result to char: %s' % res)

	def get_variable(self, name):
		try:
		   result = self._execute('GET VARIABLE', self._quote(name))
		   
		   res = result.result
		   value = result.data
		except BOAGIResultHangup:
		   res = 1
		   value = 'hangup'

		return value


	def hangup(self, channel=''):
		return self._execute('HANGUP', channel).result

	def noop(self):
		return self._execute('NOOP').result

	def receive_char(self, timeout=DEFAULT_TIMEOUT):
		res = self._execute('RECEIVE CHAR', timeout).result

		if res == 0:
			return ''
		else:
			try:
				return chr(int(res))
			except:
				raise BOAGIError('Unable to convert result to char: %s' % res)

	def record_file(self, filename, format='gsm', escape_digits='#',
			timeout=DEFAULT_RECORD, offset=0, beep='beep'):
		escape_digits = self._process_digit_list(escape_digits)
		result = self._execute('RECORD FILE', self._quote(filename), format, escape_digits, timeout, offset, beep)
		res = result.result

		try:
			return chr(int(res))
		except:
			raise BOAGIError('Unable to convert result to digit: %s' % res)

	def say_alpha(self, characters, escape_digits=''):
		characters = self._process_digit_list(characters)
		escape_digits = self._process_digit_list(escape_digits)
		res = self._execute('SAY ALPHA', characters, escape_digits).result
		
		if res == 0:
			return ''
		else:
			try:
				return chr(int(res))
			except:
				raise BOAGIError('Unable to convert result to char: %s' % res)

	def say_date(self, seconds, escape_digits=''):
		escape_digits = self._process_digit_list(escape_digits)
		res = self._execute('SAY DATE', seconds, escape_digits).result

		if res == 0:
			return ''
		else:
			try:
				return chr(int(res))
			except:
				raise BOAGIError('Unable to convert result to char: %s' % res)

	def say_datetime(self, seconds, escape_digits='', format='', zone=''):
		escape_digits = self._process_digit_list(escape_digits)
		if format: format = self._quote(format)
		res = self._execute('SAY DATETIME', seconds, escape_digits, format, zone).result
		
		if res == 0:
			return ''
		else:
			try:
				return chr(int(res))
			except:
				raise BOAGIError('Unable to convert result to char: %s' % res)

	def say_digits(self, digits, escape_digits=''):
		digits = self._process_digit_list(digits)
		escape_digits = self._process_digit_list(escape_digits)
		res = self._execute('SAY DIGITS', digits, escape_digits).result
		
		if res == 0:
			return ''
		else:
			try:
				return chr(int(res))
			except:
				raise BOAGIError('Unable to convert result to char: %s' % res)

	def say_number(self, number, escape_digits=''):
		number = self._process_digit_list(number)
		escape_digits = self._process_digit_list(escape_digits)
		res = self._execute('SAY NUMBER', number, escape_digits).result

		if res == 0:
			return ''
		else:
			try:
				return chr(int(res))
			except:
				raise BOAGIError('Unable to convert result to char: %s' % res)

	def say_phonetic(self, characters, escape_digits=''):
		characters = self._process_digit_list(characters)
		escape_digits = self._process_digit_list(escape_digits)
		res = self._execute('SAY PHONETIC', characters, escape_digits).result
		
		if res == 0:
			return ''
		else:
			try:
				return chr(int(res))
			except:
				raise BOAGIError('Unable to convert result to char: %s' % res)

	def say_time(self, seconds, escape_digits=''):
		escape_digits = self._process_digit_list(escape_digits)
		res = self._execute('SAY TIME', seconds, escape_digits).result
		if res == 0:
			return ''
		else:
			try:
				return chr(int(res))
			except:
				raise BOAGIError('Unable to convert result to char: %s' % res)

	def send_image(self, filename):
		res = self._execute('SEND IMAGE', filename).result
		
		if res != 0:
			raise BOAGIAppError('Channel falure on channel %s' % self.env.get('agi_channel','UNKNOWN'))

	def send_text(self, text=''):
		return self._execute('SEND TEXT', self._quote(text)).result

	def set_autohangup(self, secs):
		self._execute('SET AUTOHANGUP', secs)

	def set_callerid(self, number):
		self._execute('SET CALLERID', number)

	def set_context(self, context):
		self._execute('SET CONTEXT', context)

	def set_extension(self, extension):
		self._execute('SET EXTENSION', extension)

	def set_priority(self, priority):
		self._execute('SET PRIORITY', priority)

	def set_variable(self, name, value):
		self._execute('SET VARIABLE', self._quote(name), self._quote(value))

	def stream_file(self, filename, escape_digits='', sample_offset=0):
		escape_digits = self._process_digit_list(escape_digits)
		res = self.execute('STREAM FILE', filename, escape_digits, sample_offset).result

		if res == 0:
			return ''
		else:
			try:
				return chr(int(res))
			except:
				raise BOAGIError('Unable to convert result to char: %s' % res)

	def tdd_mode(self, mode='off'):
		res = self._execute('TDD MODE', mode).result
		if res == '0':
			raise BOAGIAppError('Channel %s is not TDD-capable')

	def goto_on_exit(self, context='', extension='', priority=''):
		context = context or self.protocol.env['agi_context']
		extension = extension or self.protocolenv['agi_extension']
		priority = priority or self.protocol.env['agi_priority']
		self.set_context(context)
		self.set_extension(extension)
		self.set_priority(priority)

	def verbose(self, message, level=1):
		self._execute('VERBOSE', self._quote(message), level)

	def wait_for_digit(self, timeout=DEFAULT_TIMEOUT):
		res = self._execute('WAIT FOR DIGIT', timeout).result

		if res == 0:
			return ''
		else:
			try:
				return chr(int(res))
			except ValueError:
				raise BOAGIError('Unable to convert result to digit: %s' % res)
