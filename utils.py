import BOFastAGI.common as common
import ConfigParser
import sys
import logging
import os
import threading 

def writeaccess(agi,result):
	if common.accesslog:
		channel = ''
		uid = ''
		ipaddr = ''

		if agi.env.has_key('agi_channel'):
			channel = agi.env['agi_channel']

		if agi.env.has_key('agi_uniqueid'):
			uid = agi.env['agi_uniqueid']

		if agi.env['bofastagi_client_address']:
			ipaddr = agi.env['bofastagi_client_address'][0]

		common.accesslog.info("%s - %s - %s - %s - %s - %s - %s" % (ipaddr, agi.env['agi_network_script'],result,channel,uid,agi.get_variable('CHANNEL(accountcode)'),agi.env['agi_extension']))


def loadMainConfig():
	try:
		cfname = '%s/main.config' % common.config_path
	except:
		cfname = '/etc/BOFastAGI/main.config'

	try:
		f = open(cfname,'r')
	except:
		print "Config File %s not found" % cfname 
		sys.exit()

	cp = ConfigParser.ConfigParser()
	cp.readfp(f)

	for section in cp.sections():
		if section=='general':
			try:
				common.config['bindip'] = cp.get('general','bindip')
			except:
				print "Config file %s section [general] bindip not found using '0.0.0.0' as default" % cfname
				common.config['bindip'] = '0.0.0.0'

			try:
				common.config['bindport'] = int(cp.get('general','bindport'))
			except:
				print "Config file %s section [general] bindport must be a integer using '4573' as default" % cfname
				common.config['bindport'] = 4573

			try:
				try:
					common.config['timeout'] = int(cp.get('general','timeout'))
				except ValueError:
					common.config['timeout'] = None

				if common.config['timeout'] == 0:
					common.config['timeout'] = None
			except:
				print "Config file %s section [general] timeout must be a integer using '0 (unlimited)' as default" % cfname
				common.config['timeout'] = None

			try:
				common.config['pidfile'] = cp.get('general','pidfile')
			except:
				print "Config file %s section [general] pidfile not found using '/tmp/BOFastAGI.pid' as default" % cfname
				common.config['pidfile'] = '/tmp/BOFastAGI.pid'
		
		elif section=='log':
			try:
				common.config['logfile'] = cp.get('log','file')
				common.config['loglevel'] = cp.get('log','level').upper()
			except:
				print "Config file %s section [log] file and level must be set default used" % cfname
				common.config['logfile'] = '/var/log/BOFastAGI/server.log'
				common.config['loglevel'] = 'INFO'

			try:
				common.config['accesslog'] = cp.get('log','accesslog')
				common.accesslog = logging.getLogger('access')
				common.accesslog.setLevel(logging.INFO)
				fh = logging.FileHandler(common.config['accesslog'])
				formatter = logging.Formatter('%(asctime)s - %(message)s',datefmt='%d/%m/%Y %H:%M:%S')
				fh.setFormatter(formatter)
				common.accesslog.addHandler(fh)
				common.accesslog.propagate = False
			except:
				pass

	common.admins = {'darks': 'prueba'}
