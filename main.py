#!/usr/bin/env python

from BOFastAGI.router import BOFastAGIRouter
from BOFastAGI.dbs import BODBFactory
from BOFastAGI.fastagi import BOFastAGIServer
import BOFastAGI.common as common
import logging
import os, sys, time, traceback
from daemon import Daemon
from BOFastAGI.utils import loadMainConfig
from argparse import ArgumentParser
import signal

def reload_config(signum,frame):
    common.router.reloadConfig()

class MyDaemon(Daemon):
    def run(self):
        logging.basicConfig(filename=common.config['logfile'],level=common.config['loglevel'],format='%(asctime)s - %(name)s - %(levelname)s: %(message)s',datefmt='%d/%m/%Y %H:%M:%S')
        logging.info("Server started - IP Address: [%s] - Port: [%d]" % (common.config['bindip'], common.config['bindport']))
        signal.signal(signal.SIGUSR1, reload_config)

        try:
            common.dbs = BODBFactory()
            common.router = BOFastAGIRouter()
            
            common.bofas = BOFastAGIServer(common.config['bindip'], common.config['bindport'])
            common.bofas.daemon = True
            common.bofas.start()

            while True:
                time.sleep(1)

        except KeyboardInterrupt, e:
            del(common.dbs)
            del(common.router)
            common.bofas.stop()
            raise SystemExit

        except:
            logging.info(traceback.format_exc())
    
if __name__ == "__main__":
    try:
        import setproctitle
        setproctitle.setproctitle('BOFastAGI')
    except:
        pass
        
    parser = ArgumentParser()
    parser.add_argument('-f', '--no-fork', help='no fork from console',default=0,action='store_true')
    parser.add_argument('action', help='action to execute in daemon mode', nargs='?', default='start', choices=['start','stop','restart','reload'])
    parser.add_argument('-c', '--config-path', help='path for configuration directory',default='/etc/BOFastAGI/')

    args = parser.parse_args()

    common.config_path = args.config_path

    loadMainConfig()

    if args.no_fork:
        logging.basicConfig(filename=common.config['logfile'],level=common.config['loglevel'],format='%(asctime)s - %(name)s - %(levelname)s: %(message)s',datefmt='%d/%m/%Y %H:%M:%S')
        logging.info("Server started - IP Address: [%s] - Port: [%d]" % (common.config['bindip'], common.config['bindport']) )
        signal.signal(signal.SIGUSR1, reload_config)

        try:
            common.dbs = BODBFactory()
            common.router = BOFastAGIRouter()
            
            common.bofas = BOFastAGIServer(common.config['bindip'], common.config['bindport'])
            common.bofas.daemon = True
            common.bofas.start()

            while True:
                time.sleep(1)

        except KeyboardInterrupt, e:
            del(common.dbs)
            del(common.router)
            common.bofas.stop()
            raise SystemExit

        except:
            logging.info(traceback.format_exc())
    else:
        daemon = MyDaemon(common.config['pidfile'],stderr='/tmp/BOFastAGI.err',stdout='/tmp/BOFastAGI.out')
        
        if args.action == 'start':
            daemon.start()
        elif args.action == 'stop':
            daemon.stop()
        elif args.action == 'restart':
            daemon.restart()
        elif args.action == 'reload':
            daemon.reload()
        else:
            print "Unknown command"
            sys.exit(2)
 
